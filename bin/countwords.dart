import 'dart:io';

void main() {
  print('Please input sectence :');
  String string = stdin.readLineSync()!.toLowerCase();
  List list = string.split(' ');
  var countList = list.toSet().toList();
  print('The sentence without duplicate word is :');
  var strList = countList.join(' ');
  print(strList);
  print('Total of word in sentence is :');
  print(countList.length);

  var countWord = Map();
  for (var word in list) {
    if (!countWord.containsKey(word)) {
      countWord[word] = 1;
    } else {
      countWord[word] += 1;
    }
  }
  print('Total of each word in sentence is :');
  print(countWord);
}
